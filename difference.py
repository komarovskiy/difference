import sys
import difflib
from bs4 import BeautifulSoup


def prepare_strings(iterable):
    result = []
    for s in iterable:
        value = s.strip()
        # ignoring blank lines and HTML comment lines
        if value and not value.startswith('<!--') and not value.endswith('-->'):
            result.append(value)
    return result


def get_html(selector, source):
    for el in source.select(selector):
        return el.parent.parent.__str__()


text = {}

original = open(sys.argv[1], "r")
other = open(sys.argv[2], "r")

original_bs = BeautifulSoup(original, 'html5lib')
other_bs = BeautifulSoup(other, 'html5lib')
selector = "div.panel:contains(Area) div > a"

text['original'] = prepare_strings(get_html(selector, original_bs).splitlines())
text['other'] = prepare_strings(get_html(selector, other_bs).splitlines())

d = difflib.Differ()

diff = d.compare(
    text['original'],
    text['other']
)

for line in diff:
    if line:
        print(line)
